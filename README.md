# ECE285F

## Description
Implementation of fully convolutionaly networks for semantic segmentation for the Kaggle Airbus Ship Dectection Challenge.

## Requirements
Install package 'imageio ' as follows:  
$ pip install --user imageio  

## Code organization
demo.ipynb -- Run a demo of our code (reproduce IoU scores of our report)  
vggnet/VggNet-classifier.ipynb -- Train the VGGNet classifier  
vggnet/FCN_Training.ipynb -- Run the fine-tuning training for the fully convolutional network  
vggnet/fcn_training.pth -- Parameters for VGGNet FCN  
alex/Train_Alex_FCN.ipynb -- Failed AlexNet implementation  
vgg2/ -- Failed VGGNet implementation  
util/ASDC_loader.py -- module for interfacing with the dataset  
util/ModelUtils.py -- misc utilities  
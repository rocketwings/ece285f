import csv
#import glob
import imageio
import numpy as np
import os
import torch
import torch.utils.data
import json
import urllib.request
import scipy.ndimage.measurements
import matplotlib.pyplot as plt

image_shape = (768, 768)
class ASDC_Dataset(torch.utils.data.Dataset):
    ASDC_ranges = []
    def __init__(self, path='/datasets/ee285f-public/airbus_ship_detection', 
                    train_dir='train_v2', 
                    test_dir='test_v2', 
                    dataset='train', 
                    csv_filename='train_ship_segmentations_v2.csv',
                    truncate=10000,
                    ID_idxs=None):

        super().__init__()
        self.dataset_type = dataset
        self.data_path = os.path.join(path, train_dir)

        corrupted_IDs = ['6384c3e78.jpg']
        self.e_pixels = {}  #dict of lists of EP strings
        with open(os.path.join(path, csv_filename)) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            line_count = -1 #skip first line
            id_count = 0
            for imageID, EncodedPixels in csv_reader:
                if imageID not in corrupted_IDs:
                    if line_count >=0:
                        if imageID in self.e_pixels:
                            self.e_pixels[imageID].append(EncodedPixels)
                        else:
                            self.e_pixels[imageID] = [EncodedPixels]
                            id_count += 1
                    line_count += 1
                    if truncate > 0 and id_count >= truncate:
                        break

        self.imageIDs = list(self.e_pixels.keys())
        self.imageIDs = sorted(self.imageIDs)

        self.set_range(ID_idxs)

    def set_range(self,ID_idxs):
        if ID_idxs == None:
            self.ID_idxs = range(len(self.imageIDs))
        else:
            self.ID_idxs = ID_idxs
            self.imageIDs = [ self.imageIDs[i] for i in self.ID_idxs ]
        ASDC_Dataset.ASDC_ranges.append((self.dataset_type, self.ID_idxs))


    def get_image(self, imageID):
        if '.jpg' in imageID:
            full_filename = os.path.join(self.data_path, imageID)
        else:
            print('warning, imageID missing ".jpg"')
            full_filename = os.path.join(self.data_path, imageID+ '.jpg')
        image = imageio.imread(full_filename)
        image = image/255.0
        return image    #returns [w,h,c] imageio object

    def __len__(self):
        return len(self.imageIDs)

    def __getitem__(self, index):
        imageID = self.imageIDs[index]
        full_filename = os.path.join(self.data_path, imageID)
        image = self.get_image(imageID)
        image = np.moveaxis(image,[0,1,2], [1,2,0])
        mask = False
        for ep in self.e_pixels[imageID]:
            mask = np.logical_or(mask, ep2mask(ep))
        mask = np.expand_dims(mask, axis=0).astype(float)
        return torch.FloatTensor(image), torch.FloatTensor(mask)
    
    def getID(self,index):
        imageID = self.imageIDs[index]
        return imageID

    def __debug_print__(self):
        print(self.imageIDs[0:10])
        pass

class ASDC_Class_Dataset(ASDC_Dataset):
    def __getitem__(self, index):
        imageID = self.imageIDs[index]
        #full_filename = os.path.join(self.data_path, imageID)
        #image = torch.FloatTensor(imageio.imread(full_filename))
        image = self.get_image(imageID)
        image = np.moveaxis(image,[0,1,2], [1,2,0]) # to channel, width, height
        if self.e_pixels[imageID] == '':
            is_ship = 0
        else:
            is_ship = 1
        return torch.FloatTensor(image), torch.FloatTensor((is_ship, not is_ship))

class ASDC_Crop_Class_Dataset(ASDC_Dataset):
    def __init__(self, crop=(0,0),**kwargs):
        super(ASDC_Crop_Class_Dataset, self).__init__(**kwargs)
        self.crop = crop
        self.crops = []
        for key in self.imageIDs:
            #if self.e_pixels[key] == ['']:
            #    self.crops.append((key, -1)) #append imageID, ep index
            #else:
            for i in range(len(self.e_pixels[key])):
                self.crops.append((key, i))
    def __len__(self):
        return len(self.crops)
    def get_mask(self, index):
        imageID, idx = self.crops[index]
        ship_mask = ep2mask(self.e_pixels[imageID][idx])
        if np.min(ship_mask) == np.max(ship_mask):
            centroid = None
        else:
            centroid = scipy.ndimage.measurements.center_of_mass(ship_mask)

        for ep in self.e_pixels[imageID]:    #add all ships to mask, even uncentered ones
            ship_mask = np.logical_or(ship_mask, ep2mask(ep))
        ship_mask = np.expand_dims(ship_mask, axis=0)
        cropped_mask =  crop_torch_image(ship_mask, self.crop, centroid=centroid)
        return cropped_mask
    def getID(self, index):
        imageID, idx = self.crops[index]
        return imageID
    def __getitem__(self, index):
        imageID, idx = self.crops[index]
        image = self.get_image(imageID)
        image = np.moveaxis(image,[0,1,2], [1,2,0]) # to channel, width, height
        ship_mask = ep2mask(self.e_pixels[imageID][idx])
        if np.min(ship_mask) == np.max(ship_mask):
            centroid = None
        else:
            centroid = scipy.ndimage.measurements.center_of_mass(ship_mask)

        for ep in self.e_pixels[imageID]:    #add all ships to mask, even uncentered ones
            ship_mask = np.logical_or(ship_mask, ep2mask(ep))
        ship_mask = np.expand_dims(ship_mask, axis=0)   #get into tensor dimensions for cropping

        cropped_image = crop_torch_image(image, self.crop, centroid=centroid)
        cropped_mask =  crop_torch_image(ship_mask, self.crop, centroid=centroid)

        if cropped_mask.any():
            is_ship = 1
        else:
            is_ship = 0

        return torch.FloatTensor(cropped_image), is_ship

class ASDC_Crop_Mask2_Dataset(ASDC_Crop_Class_Dataset):
    def __getitem__(self, index):
        imageID, idx = self.crops[index]
        image = self.get_image(imageID)
        image = np.moveaxis(image,[0,1,2], [1,2,0]) # to channel, width, height
        ship_mask = ep2mask(self.e_pixels[imageID][idx])
        if np.min(ship_mask) == np.max(ship_mask):
            centroid = None
        else:
            centroid = scipy.ndimage.measurements.center_of_mass(ship_mask)

        for ep in self.e_pixels[imageID]:    #add all ships to mask, even uncentered ones
            ship_mask = np.logical_or(ship_mask, ep2mask(ep))
        ship_mask = np.expand_dims(ship_mask, axis=0)   #get into tensor dimensions for cropping

        cropped_image = crop_torch_image(image, self.crop, centroid=centroid)
        cropped_mask =  crop_torch_image(ship_mask, self.crop, centroid=centroid)

        cropped_ship_mask = cropped_mask
        cropped_back_mask = np.abs(cropped_ship_mask-1)
        mask = np.concatenate((cropped_ship_mask, cropped_back_mask), axis=0)

        return torch.FloatTensor(cropped_image), torch.FloatTensor(mask)
    
class ASDC_Crop_Mask1_Dataset(ASDC_Crop_Class_Dataset):
    def __getitem__(self, index):
        imageID, idx = self.crops[index]
        image = self.get_image(imageID)
        image = np.moveaxis(image,[0,1,2], [1,2,0]) # to channel, width, height
        ship_mask = ep2mask(self.e_pixels[imageID][idx])
        if np.min(ship_mask) == np.max(ship_mask):
            centroid = None
        else:
            centroid = scipy.ndimage.measurements.center_of_mass(ship_mask)

        for ep in self.e_pixels[imageID]:    #add all ships to mask, even uncentered ones
            ship_mask = np.logical_or(ship_mask, ep2mask(ep))
        ship_mask = np.expand_dims(ship_mask, axis=0)   #get into tensor dimensions for cropping

        cropped_image = crop_torch_image(image, self.crop, centroid=centroid)
        cropped_mask =  crop_torch_image(ship_mask, self.crop, centroid=centroid)

        cropped_ship_mask = cropped_mask
        
        mask = cropped_ship_mask.astype(int)
        

        
        return torch.FloatTensor(cropped_image), torch.LongTensor(mask)
    

class ASDC_Mask_Dataset(ASDC_Dataset):
    pass

class ASDC_Mask2_Dataset(ASDC_Dataset):
    def __getitem__(self, index):
        imageID = self.imageIDs[index]
        full_filename = os.path.join(self.data_path, imageID)
        image = self.get_image(imageID)
        image = np.moveaxis(image,[0,1,2], [1,2,0])
        ship_mask = False
        for ep in self.e_pixels[imageID]:    #add all ships to mask, even uncentered ones
            ship_mask = np.logical_or(ship_mask, ep2mask(ep))
        back_mask = np.abs(ship_mask-1)
        mask = np.stack((ship_mask, back_mask), axis=0)
        return torch.FloatTensor(image), torch.FloatTensor(mask)

def ep2mask(epixels):    #encoded pixels to numpy mask
    mask = np.zeros(image_shape)
    if epixels == '':
        return mask
    else:
        nums = [int(word) for word in epixels.split(' ')]
        nums = np.array(nums)
        nums = np.reshape(nums, (-1,2))
        ends = nums.sum(axis=1)
        try:
            for r in range(nums.shape[0]):
                mask[np.unravel_index(np.arange(nums[r,0]-1, ends[r]-1), image_shape)] = 1
            return mask.T
        except ValueError:
            print('ValueError')
            print(r)
            print(nums[r,:])
            print(ends[r])
            print(epixels)
            raise ValueError
def torch2regular_image(image):
    return np.squeeze(np.moveaxis(image.numpy(),[1,2,0], [0,1,2] ))

def get_IN_labels():
    url = 'https://s3.amazonaws.com/deep-learning-models/image-models/imagenet_class_index.json'
    filename = 'imagenet_class_index.json'
    try:
        with open(filename) as f:
            class_idx = json.load(f)
    except:
        urllib.request.urlretrieve(url, filename)
        #with open(filename, 'wb') as f:
        #    f.write(datatowrite)
        with open(filename) as f:
            class_idx = json.load(f)

    idx2label = [class_idx[str(k)][1] for k in range(len(class_idx))]
    return idx2label    #returns list of strings

def crop_torch_image(torch_image, crop_size, centroid=None, padding='shift'):
    if len(torch_image.shape) == 3:
        full_size = torch_image.shape[1:3]
    elif len(torch_image.shape) == 4:
        full_size = torch_image.shape[2:4]
    if centroid == None:
        centroid = np.floor(np.array(full_size)/2.0)

    r = np.round(centroid[0]) + np.array([-np.floor(crop_size[0]/2.0), np.ceil(crop_size[0]/2.0)])
    c = np.round(centroid[1]) + np.array([-np.floor(crop_size[1]/2.0), np.ceil(crop_size[1]/2.0)])
    r = r.astype(int)
    c = c.astype(int)

    if padding == 'shift':  #shifts cropped region to remain inside the full image
        if r[0] < 0:
            r[1] = r[1] - r[0]
            r[0] = 0
        elif r[1] > full_size[0]:
            r[0] = r[0] - (r[1] - full_size[0])
            r[1] = full_size[0]
        if c[0] < 0:
            c[1] = c[1] - c[0]
            c[0] = 0
        elif c[1] > full_size[1]:
            c[0] = c[0] - (c[1] - full_size[1])
            c[1] = full_size[1]
        #r = np.clip(r, 0, full_size[0]).astype(int)
        #c = np.clip(c, 0, full_size[1]).astype(int)
        if len(torch_image.shape) == 3:
            cropped_image = torch_image[:,r[0]:r[1],c[0]:c[1]]
        elif len(torch_image.shape) == 4:
            cropped_image = torch_image[:,:,r[0]:r[1],c[0]:c[1]]
    elif padding == 'extend':
        raise NotImplementedError

    if cropped_image.shape[-2:] != crop_size:
        print('ERROR:')
        print(r)
        print(c)
        print(centroid)
        assert cropped_image.shape[-2:] != crop_size, 'output image is not the right size'

    return cropped_image

def show_batch(torch_image,idx, flag, mask=None, show=True):
    cols = 2
    rows = 1 if len(torch_image.shape)==3 else torch_image.shape[0]
    fig = plt.figure()
    plt.subplot(rows,cols,1)
    
    for i in range(rows):
        plt.subplot(rows,cols,i*2+1)
        if len(torch_image.shape) == 4:
            plt.imshow(torch2regular_image(torch_image[i,:,:,:]))
        else:
            plt.imshow(torch2regular_image(torch_image))
        if mask is not None:
            plt.subplot(rows,cols,i*2+2)
            if len(torch_image.shape) == 4:
                plt.imshow(mask[i,0,:,:])
            else:
                plt.imshow(mask[0,:,:])
                
    plt.savefig("./Results/"+ flag + "{}.png".format(idx))
    
    if show:
        plt.show()
        
    return fig


def save_checkpoint(path, **kwargs):
    checkpoint_dict = {}
    try:
        model = kwargs['model']
        checkpoint_dict.update({'model_state_dict': model.state_dict(),
                                'model_type': type(model),
                                })
    except:
        print('warning: no model')
        pass
    try:
        optimizer = kwargs['optimizer']
        checkpoint_dict.update({'optimizer_state_dict': optimizer.state_dict(),
                                'optimizer_type': type(optimizer)
                                })
    except:
        print('warning: no optimizer')
        pass

    for key in kwargs:
        if key!='model' and key!='optimizer':
            checkpoint_dict.update({key:kwargs[key]})

    torch.save(checkpoint_dict, path)

def load_checkpoint(path, model=None, optimizer=None):
    checkpoint_dict = torch.load(path)
    try:
        model.load_state_dict(checkpoint_dict['model_state_dict'])
    except:
        pass
    try:
        optimizer.load_state_dict(checkpoint_dict['optimizer_state_dict'])
    except:
        pass
    return checkpoint_dict
if __name__=='__main__':
    debug = ASDC_Dataset()
    debug.__debug_print__()
    print(type(debug[0]))
    
    pass

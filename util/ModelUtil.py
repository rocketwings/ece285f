import torch
import numpy as np


def make_val_ranges(max_idx, num_sets, current_num, rand=None):
    end = max_idx
    cur = current_num # range of [0, n)
    if rand is None:
        seg_len = int(np.ceil(end/num_sets))
        train_range = list(range(0, (cur)*seg_len)) + list(range((cur+1)*seg_len, max_idx))
        val_range = range((cur)*seg_len, end if cur==num_sets-1 else (cur+1)*seg_len)
        return train_range, val_range
    else:
        raise NotImplementedError

def onehot2label(d):
    lbl = d.argmax(dim=1)
    return lbl

def eval_class_dataset(model, loader, device):
    model.eval()
    num_correct = 0.0
    for idx, (images, labels) in enumerate(loader):
        images = images.to(device)
        labels = labels.to(device)
        pred = model(images)
        pred_labels = onehot2label(pred)
        num_correct += torch.sum(labels==pred_labels)
#         if idx%100 == 99:
    #         print("{:d}: {:f}".format(idx, num_correct/num_total))
#              print("{:d}: {:f}".format(idx, num_correct))
    accuracy = float(num_correct)/len(loader.dataset)
    model.train()
    return accuracy

def save_checkpoint(path, **kwargs):
    checkpoint_dict = {}
    try:
        model = kwargs['model']
        checkpoint_dict.update({'model_state_dict': model.state_dict(),
                                'model_type': type(model),
                                })
    except:
        print('warning: no model')
        pass
    try:
        optimizer = kwargs['optimizer']
        checkpoint_dict.update({'optimizer_state_dict': optimizer.state_dict(),
                                'optimizer_type': type(optimizer)
                                })
    except:
        print('warning: no optimizer')
        pass

    for key in kwargs:
        if key!='model' and key!='optimizer':
            checkpoint_dict.update({key:kwargs[key]})

    torch.save(checkpoint_dict, path)

def load_checkpoint(path, model=None, optimizer=None):
    checkpoint_dict = torch.load(path)
    try:
        if type(model)==checkpoint_dict['model_type']:
            model.load_state_dict(checkpoint_dict['model_state_dict'])
        elif model is None:
            pass
        else:
            raise Exception('model given does not match model in checkpoint file')
    except KeyError:
        print('no model in checkpoint file')
        pass
    try:
        if type(optimizer)==checkpoint_dict['optimizer_type']:
            optimizer.load_state_dict(checkpoint_dict['optimizer_state_dict'])
        elif optimizer is None:
            pass
        else:
            raise Exception('optimizer given does not match optimizer in checkpoint file')
    except KeyError:
        print('no optimizer in checkpoint file')
        pass
    return checkpoint_dict


def save_whole_checkpoint():
    ASDC_loader.save_checkpoint(param_path, 
                                model=model, 
                                optimizer=optimizer, 
                                epoch=epoch, 
                                idx=idx, 
                                B=B, 
                                losses=losses, 
                                train_accs=train_accs,
                                val_accs=val_accs)
